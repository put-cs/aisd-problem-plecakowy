from itertools import combinations
from random import randint, sample
from operator import attrgetter
from os import system
from time import process_time
from time import perf_counter
from decimal import Decimal

class Item:
    def __init__(self, index, weight, value):
        self.index = index
        self.weight = weight
        self.value = value
        self.ratio = self.value/self.weight

def validCombinations(items, capacity):
    unfiltered = []
    filtered = []
    for r in range(1,len(items)+1):
        for comb in combinations(items, r):
            unfiltered.append(list(comb))

    for combination in unfiltered:
        weight_sum = 0
        for item in combination:
            weight_sum += item.weight
        if weight_sum <= capacity:
            filtered.append(combination)
    return filtered

def allCombinations(items):
    out = []
    for r in range(1,len(items)+1):
        for comb in combinations(items, r):
            out.append(list(comb))
    return out

def printItem(item):
    print(f"Index: {item.index}, Weight: {item.weight}, Value: {item.value}, Ratio: {item.ratio}")

def dynamicKnapSackIterative(capacity, items):
    n = len(items)
    table = [[0 for x in range(capacity + 1)] for x in range(n + 1)]

    for i in range(n + 1):
        for j in range(capacity + 1):
            if i == 0 or j == 0:
                table[i][j] = 0
            elif items[i-1].weight <= j:
                table[i][j] = max(items[i-1].value + table[i-1][j-items[i-1].weight],  table[i-1][j])
            else:
                table[i][j] = table[i-1][j]
    return table[n][capacity]

def dynamicKnapSackRecursive(n, capacity, items):
    if capacity == 0 or n == 0:
        return 0
    if items[n-1].weight > capacity:
        return dynamicKnapSackRecursive(n-1, capacity, items)
    else:
        return max(items[n-1].value + dynamicKnapSackRecursive(n-1, capacity-items[n-1].weight, items), dynamicKnapSackRecursive(n-1, capacity, items))

def initData(samplesize):
    items = []
    min_value = 100; max_value = 10000
    min_weight = 10; max_weight = 1000

    for index in range(0,samplesize):
        items.append(Item(index, randint(min_weight, max_weight), randint(min_value, max_value)))
    return items

def printCombination(combinations):
    for combination in combinations:
        for item in combination:
            printItem(item)
        print("")

def printItems(items):
    for item in items:
        printItem(item)

def findMax(combinations, capacity):
    max_sum = 0
    max_combination = []

    for combination in combinations:
        current_sum = 0
        weight_sum = 0
        for item in combination:
            current_sum += item.value
            weight_sum += item.weight
        if weight_sum <= capacity:
            if current_sum > max_sum:
                max_sum = current_sum
                max_combination = combination
    return max_sum

def initDebugData(samplesize):
    items = []
    items.append(Item(0, 5, 3))
    items.append(Item(1, 3, 4))
    items.append(Item(2, 2, 2))
    items.append(Item(3, 4, 6))
    items.append(Item(4, 3, 1))
    return items

def heuristicByValue(items, capacity):
    # print("")
    output = []
    sorted_items = sorted(items, key=attrgetter('value'), reverse=False)
    # printItems(sorted_items)
    weight_sum = 0
    max_value = 0
    while weight_sum <= capacity:
        try:
            output.append(sorted_items.pop())

            weight_sum += output[len(output)-1].weight
            max_value += output[len(output)-1].value
            added_value = output[len(output)-1].value

            if weight_sum > capacity:
                output.pop()
                max_value -= added_value
                break
        except IndexError:
            break
    return max_value

def heuristicByWeight(items, capacity):
    output = []
    sorted_items = sorted(items, key=attrgetter('weight'), reverse=True)
    weight_sum = 0
    max_value = 0
    while weight_sum <= capacity:
        try:
            output.append(sorted_items.pop())

            weight_sum += output[len(output)-1].weight
            max_value += output[len(output)-1].value
            added_value = output[len(output)-1].value

            if weight_sum > capacity:
                output.pop()
                max_value -= added_value
                break
        except IndexError:
            break
    return max_value

def heuristicByRatio(items, capacity):
    output = []
    sorted_items = sorted(items, key=attrgetter('ratio'), reverse=False)
    # printItems(sorted_items)
    weight_sum = 0
    max_value = 0
    while weight_sum <= capacity:
        try:
            output.append(sorted_items.pop())

            weight_sum += output[len(output)-1].weight
            max_value += output[len(output)-1].value
            added_value = output[len(output)-1].value

            if weight_sum > capacity:
                output.pop()
                max_value -= added_value
                break
        except IndexError:
            break
    return max_value

def heuristicRandom(items, capacity):
    output = []
    shuffled_items = sample(items, len(items))
    weight_sum = 0
    max_value = 0
    while weight_sum <= capacity:
        try:
            output.append(shuffled_items.pop())

            weight_sum += output[len(output)-1].weight
            max_value += output[len(output)-1].value
            added_value = output[len(output)-1].value

            if weight_sum > capacity:
                output.pop()
                max_value -= added_value
                break
        except IndexError:
            break
    return max_value

def getNewCapacity(items, weight_ratio):
    weight = 0
    for item in items:
        weight += item.weight
    return round(weight * weight_ratio)

def main():
    system("rm tempCodeRunnerFile.py 2> /dev/null")
    system("mkdir results 2> /dev/null")

    pd_25 = open("results/pd_25", "w")
    pd_50 = open("results/pd_50", "w")
    pd_75 = open("results/pd_75", "w")

    bf1_25 = open("results/bf1_25", "w")
    bf1_50 = open("results/bf1_50", "w")
    bf1_75 = open("results/bf1_75", "w")

    bf2_25 = open("results/bf2_25", "w")
    bf2_50 = open("results/bf2_50", "w")
    bf2_75 = open("results/bf2_75", "w")

    gh4_25 = open("results/gh4_25", "w")
    gh4_50 = open("results/gh4_50", "w")
    gh4_75 = open("results/gh4_75", "w")

    # gh1_25 = open("gh1_25", "w")
    # gh1_50 = open("gh1_50", "w")
    # gh1_75 = open("gh1_75", "w")

    # gh2_25 = open("gh2_25", "w")
    # gh2_50 = open("gh2_50", "w")
    # gh2_75 = open("gh2_75", "w")

    # gh3_25 = open("gh3_25", "w")
    # gh3_50 = open("gh3_50", "w")
    # gh3_75 = open("gh3_75", "w")

    n = [10]
    weight_ratios = [0.25, 0.50, 0.75]

    for samplesize in n:
        for weight_ratio in weight_ratios:
            items = initData(samplesize)
            capacity = getNewCapacity(items, weight_ratio)

            valid_combinations = validCombinations(items, capacity)
            all_combinations = allCombinations(items)

            dynamic_time = process_time()
            max_value_dynamic = dynamicKnapSackRecursive(samplesize, capacity, items)
            dynamic_time = process_time() - dynamic_time

            exhaustive_all_time = process_time()
            max_value_exhaustive_all = findMax(all_combinations, capacity)
            exhaustive_all_time = process_time() - exhaustive_all_time

            exhaustive_valid_time = process_time()
            max_value_exhaustive_valid = findMax(valid_combinations, capacity)
            exhaustive_valid_time = process_time() - exhaustive_valid_time

            heuristic_weight_time = process_time()
            max_value_heuristic_weight = heuristicByWeight(items, capacity)
            heuristic_weight_time = process_time() - heuristic_weight_time

            heuristic_value_time = process_time()
            max_value_heuristic_value = heuristicByValue(items, capacity)
            heuristic_value_time = process_time() - heuristic_value_time

            heuristic_ratio_time = process_time()
            max_value_heuristic_ratio = heuristicByRatio(items, capacity)
            heuristic_ratio_time = process_time() - heuristic_ratio_time

            heuristic_random_time = process_time()
            max_value_heuristic_ratio = heuristicByRatio(items, capacity)
            heuristic_random_time = process_time() - heuristic_random_time

            # print("Dynamic programming: ", max_value_dynamic)
            # print("Exhaustive search without verification: ", max_value_exhaustive_all)
            # print("Exhaustive search with verification: ", max_value_exhaustive_valid)
            # print("Heuristic by value: ", heuristicByValue(items, capacity))
            # print("Heuristic by weight: ", heuristicByWeight(items, capacity))
            # print("Heuristic by ratio: ", heuristicByRatio(items, capacity))
            # print("Heuristic by random: ", heuristicRandom(items, capacity))
            # print("\n")

            if weight_ratio == 0.25:
                pd_25.write(str(samplesize) + " " + str(round(dynamic_time, 8)))
                bf1_25.write(str(samplesize) + " " + str(round(exhaustive_all_time, 8)))
                bf2_25.write(str(samplesize) + " " + str(round(exhaustive_valid_time, 8)))
                gh4_25.write(str(samplesize) + " " + str(round(Decimal(heuristic_ratio_time), 8)))
            if weight_ratio == 0.50:
                pd_50.write(str(samplesize) + " " + str(round(dynamic_time, 8)))
                bf1_50.write(str(samplesize) + " " + str(round(exhaustive_all_time, 8)))
                bf2_50.write(str(samplesize) + " " + str(round(exhaustive_valid_time, 8)))
                gh4_50.write(str(samplesize) + " " + str(round(Decimal(heuristic_ratio_time), 8)))
            if weight_ratio == 0.75:
                pd_75.write(str(samplesize) + " " + str(round(dynamic_time, 8)))
                bf1_75.write(str(samplesize) + " " + str(round(exhaustive_all_time, 8)))
                bf2_75.write(str(samplesize) + " " + str(round(exhaustive_valid_time, 8)))
                gh4_75.write(str(samplesize) + " " + str(round(Decimal(heuristic_ratio_time), 8)))

            print(f"\nRUNNING WITH {weight_ratio}")
            print("Dynamic programming: ",round(dynamic_time, 8))
            print("Exhaustive search without verification: ", round(exhaustive_all_time,8))
            print("Exhaustive search with verification: ",round(exhaustive_valid_time,8))
            print("Heuristic by weight: ",round(Decimal(heuristic_weight_time), 8))
            print("Heuristic by value: ",round(Decimal(heuristic_value_time), 8))
            print("Heuristic by ratio: ",round(Decimal(heuristic_ratio_time), 8))
            print("Heuristic by random: ", round(Decimal(heuristic_random_time),8))


main()
